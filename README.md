# React-Native-Errors-Guide (All errors with their solutions in one place).

#### This document will guide everyone for solutions of the major errors faced while setting up the environment of React-native on PC or MAC. It also includes some more important errors which are must to figure out after setting up the environment.
##### Note: The pattern will be:
+ An Error
+ Solution

## How to install react native on a fresh project ?
+ Open terminal and run below commands.

1. /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

2. brew install node

3. brew install watchman

4. npm install -g react-native-cli

5. react-native init (your project name)

6. cd MyProject react-native run-ios

+ Optional: make a folder and then run all of the above commands on terminal.

### Error: If the metro is not starting automatically, then close the metro, and then go to the terminal of the react native in visual studio or another code editor and type there the following commands:
+	npm install
+	react-native start
+	react-native run-ios (for iOS simulator running)

### Error: For running react native on Android and Iphone simulators use the following commands:
+	react-native run-ios (for iOS simulator running)
+	react-native run-android  ..... for running android simulator as well as on actual android device
+	react-native run-ios --device (your iphon name) ..... for running the application on your actual Iphone device

### Error: Glog : configure: WARNING: 'missing' script is too old or missing.
* Run the below command:
1. sudo xcode-select --switch /Applications/Xcode.app

### Error: CocoaPods permission denied (Errno::EACCES - Permission denied @ dir_s_mkdir)
* After fresh installation, if you are not able to install "pod install" in the ios folder of the react-native, so what you need to do is make a folder in the directory by the name of CocoaPods (Add CocoaPods folder on ~/Library/Caches/CocoaPods), and run below commands on terminal.
   ###### Give full permission on your project
1.	cd your-project
2.	sudo chmod -R 777 .
3.	cd ios
4.	pod install

### Error: When you can’t able to run this command “react-native run-ios”, (simulator error).
+ For connection of our project with the IOS.
1. sudo xcode-select -s /Applications/Xcode.app
2. xcode-select –install
3. sudo xcodebuild -license accept 

###	Error code 65.
1.	cd ios  (directory entry)
2.	Pod install
3.	cd ..  (go back)

### Adding Sdk directory in order to setup react native project on Android: •	
+ Create a file by the name of "local.properties" in the android folder of your project and copy the below statement into that file and save it.
1.	sdk.dir = /Users/your Computer Name  or Username of your computer/Library/Android/sdk

###  Error: What went wrong: Could not initialize class org.codehaus.groovy.runtime.InvokerHelper
1. Go to /android/gradle/wrapper/gradle-wrapper.properties file in your React Native project
2. Find this line that starts with distributionUrl. Change the gradle version into gradle-6.3-all.zip.

+ In my project, it was: distributionUrl=https\://services.gradle.org/distributions/gradle-6.0.1-all.zip, and I changed it into: distributionUrl=https\://services.gradle.org/distributions/gradle-6.3-all.zip



### Error: android studio licenses not accepted.
+ Solution: navigate to this folder through command line of visual studio: ~/Library/Android/sdk/tools/bin
1. Then run this command
2.	./sdkmanager –licenses (sdkmanager is a .exe file and we are giving command to the exe file as licenses).
3.	“./” is used for running the executable file.

###	Error: If you face problem running android emulator (React Native adb reverse ENOENT)
+ To create a bash profile follow these commands:
1.	Start up Terminal.
2.	Type "cd ~/" to go to your home folder.
3.	Type ( touch .bash_profile ) to create your new file.
4.	Edit ( .bash_profile ) with your favorite editor or you can just type ( open -e .bash_profile ) to open it in TextEdit.


5. Open your bash profile: open .bash_profile
6. Add this to your bash_profile:
+  export ANDROID_SDK=/Users/your_computer-location_name/Library/Android/sdk
+  export PATH=/Users/your_computer_name/Library/Android/sdk/platform-tools:$PATH

7. Save and close
8. Compile your changes: source ~/.bash_profile

+ hint: Your computer means your location first hold(command and space) and add /Users you will see your location name, for example “sabawoon” and add it in the url as 
(export ANDROID_SDK=/Users/sabawoon/Library/Android/sdk)


###	Error:If you get this error  React-native, “Native module cannot be null”
1. cd ios
2. pod install


## Finding out your project name (if required). (Firebase)
1.	Click on android folder 
2.	Then click on the app folder and then on “src” folder
3.	After step 2, click on the Menifest.xml file (your package name would be mention there as (com.somthing.something)=> 
4.	So you can find  projectname/android/app/src/manifest.xml 

##  how to find your fingerprint/certificate of SHA-1(secure hash algorithm-1) in android, it is needed for firebase in order for the use of the authentication services of google like google sign in ….. (firebase)
1.	Write this in mac terminal : 

* keytool -list -v \ -alias androiddebugkey -keystore ~/.android/debug.keystore

* It will ask for password? (For_Android)
  Password: android

### Error: Facing issue “ Failed to install the app. Make sure you have the Android development environment ” in react native.
1.  This might be a problem of the dependencies, you have to add the dependencies in the app/build.gradle file not in the project gradle file.
*	For solving such kinds of error you have to process your project in the Android Studio app.

### For removing your node-modules and re-installing it again.(use for metro bundle operations)(optional).
1.	rm -rf node_modules/
2.	npm install

### Error: If you get error in Android emulator as 500.
* It means that the code is not accessing the files which you have given e.g images locations should be set accurately.

### Error: Camera Permissons
1. For permissions of the camera and audio etc you have to add it in app/build gradle.
     <uses-permission android:name="android.permission.CAMERA" />  <br>
     <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" /> <br>
    
     <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" /> <br>
     <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" /> <br>
     <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" /> <br>
     <uses-permission android:name="android.permission.RECORD_AUDIO"/> <br>
    
For Firebase, this is important to go for google 4.2.0 services as it might solve many of the problems.
  *  classpath 'com.google.gms:google-services:4.2.0'
  
Similarly for resolving camera error you have to add this at the top: cannot resolve camera error.

default config{

//cannot resolve camera error <br>
         missingDimensionStrategy 'react-native-camera', 'general'
         <br>
}

  
### Error: For the DexArchive error:    (react native com.android.builder.dexing.DexArchiveMergerException: Error while merging dex archives: The number of method references in a .dex file cannot exceed 64K.)
1. you have to add this in app/buld.gradle. 

Default config{<br>


// dex archive error solved by this
       
         multiDexEnabled true
<br>}













